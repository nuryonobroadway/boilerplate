-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "admins" (
   "id" uuid PRIMARY KEY,
    "username" VARCHAR (30) UNIQUE,
    "password" VARCHAR (100),
    "admin_key" TEXT,
    "role_id" INT REFERENCES roles (id),

    "created_at"      timestamp DEFAULT now(),
    "updated_at"      timestamp DEFAULT now(),
    "deleted_at"      timestamp DEFAULT NULL
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "admins";
-- +goose StatementEnd
