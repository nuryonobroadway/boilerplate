-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "attends" (
    "id" uuid PRIMARY KEY,
    "user_id" uuid REFERENCES users (id),
    "meeting_id" varchar REFERENCES meetings (id),
    "role_id" INT REFERENCES roles (id),
    "access_id" text,
    "created_at"      timestamp        not null,
    "updated_at"      timestamp        not null,
    "deleted_at"      timestamp
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "attends";
-- +goose StatementEnd
