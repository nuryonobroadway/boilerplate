-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "meetings" (
    "id" varchar PRIMARY KEY,
    "access_id" TEXT,
    "room_name" varchar UNIQUE,
    "room_password" varchar DEFAULT NULL,
    "created_at"      timestamp DEFAULT now(),
    "updated_at"      timestamp DEFAULT now(),
    "deleted_at"      timestamp DEFAULT NULL
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "meetings";
-- +goose StatementEnd
