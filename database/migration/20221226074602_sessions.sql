-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "sessions" (
    "id" uuid PRIMARY KEY,
    "user_id" uuid REFERENCES users (id),
    "login_at" timestamp DEFAULT now() ,
    "last_activity" timestamp DEFAULT now(),

    "created_at"      timestamp        not null,
    "updated_at"      timestamp        not null,
    "deleted_at"      timestamp
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "sessions";
-- +goose StatementEnd
