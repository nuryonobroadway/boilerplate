-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "meeting_logs" (
    "id" uuid PRIMARY KEY,
    "user_id" uuid REFERENCES users (id),
    "meeting_id" varchar REFERENCES meetings (id),
    "meeting_name" varchar,
    "done_at" timestamp DEFAULT now(),
    "created_at"      timestamp DEFAULT now(),
    "updated_at"      timestamp DEFAULT now(),
    "deleted_at"      timestamp DEFAULT NULL
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "meetings";
-- +goose StatementEnd
