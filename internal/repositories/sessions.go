package repositories

import (
	"context"
	"database/sql"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) InsertSession(ctx context.Context, param *entity.User) (*entity.Session, error) {
	session := &entity.Session{
		ID:           uuid.New(),
		UserID:       param.ID,
		LoginAt:      time.Now(),
		LastActivity: time.Now(),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    time.Time{},
	}

	q := `INSERT INTO ` + Session_table + ` (id, user_id, login_at, last_activity, created_at, updated_at, deleted_at) 
			VALUES ($1,$2,$3,$4,$5,$6,$7)`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return nil, err
	}

	values := []interface{}{
		session.ID,
		session.UserID,
		session.LoginAt,
		session.LastActivity,
		session.CreatedAt,
		session.UpdatedAt,
		session.DeletedAt,
	}

	_, err = tx.Exec(q, values...)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	tx.Commit()

	return session, nil
}

func (c *client) InsertSessionAndTokens(ctx context.Context, param *entity.User, access_token string, exp time.Time) (*entity.Session, error) {
	session := &entity.Session{
		ID:           uuid.New(),
		UserID:       param.ID,
		LoginAt:      time.Now(),
		LastActivity: time.Now(),
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    time.Time{},
	}

	token := &entity.Tokens{
		ID:          uuid.New(),
		AccessToken: access_token,
		ExpiredAt:   exp,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		DeletedAt:   time.Time{},
	}

	/*
		WITH resultId(id) as (
			INSERT INTO
				sessions as u (id, user_id, login_at, last_activity, created_at, updated_at, deleted_at)
			VALUES
				('fa2185e4-fd76-425c-98fe-389424991a3f', 'fa2185e4-fd76-425c-98fe-389424991a3f', now(), now(), now(), now(), now()) RETURNING u.id
		)
		INSERT INTO
			tokens (id, session_id, access_token, created_at, updated_at, deleted_at)
		VALUES ('fa2185e4-fd76-425c-98fe-389424991a3f', (SELECT resultId.id FROM resultId), '1233344', now(), now(), now())
		RETURNING (SELECT resultId.id FROM resultId)
	*/

	q := `WITH resultId(id) as (
		INSERT INTO
			` + Session_table + ` as u (id, user_id, login_at, last_activity, created_at, updated_at, deleted_at)
		VALUES
			($1, $2, $3, $4, $5, $6, $7) RETURNING u.id
		)
		INSERT INTO
			` + Token_table + ` (id, session_id, access_token, expired_at, created_at, updated_at, deleted_at)
		VALUES ($8, (SELECT resultId.id FROM resultId), $9, $10, $11, $12, $13)`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return nil, err
	}

	values := []interface{}{
		session.ID,
		session.UserID,
		session.LoginAt,
		session.LastActivity,
		session.CreatedAt,
		session.UpdatedAt,
		session.DeletedAt,
		token.ID,
		token.AccessToken,
		token.ExpiredAt,
		token.CreatedAt,
		token.UpdatedAt,
		token.DeletedAt,
	}

	_, err = tx.Exec(q, values...)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	tx.Commit()

	return session, nil
}

func (c *client) QuerySession(ctx context.Context, query string, args ...interface{}) ([]*entity.Session, error) {
	session := []*entity.Session{}

	err := c.db.Fetch(ctx, &session, query, args...)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return session, nil
}

func (c *client) GetSession(ctx context.Context, user_id uuid.UUID) ([]*entity.Session, error) {
	q := `SELECT * FROM ` + Session_table + ` WHERE user_id = $1`
	return c.QuerySession(ctx, q, user_id)
}

func (c *client) UpdateSession(ctx context.Context, id uuid.UUID) error {
	q := `UPDATE ` + Session_table + ` SET last_activity = $1, updated_at = $2 WHERE id = $3`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return err
	}

	args := []interface{}{
		time.Now(),
		time.Now(),
		id,
	}

	_, err = tx.Exec(q, args...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (c *client) DeleteSession(ctx context.Context, id uuid.UUID) error {
	q := ` DELETE FROM ` + Session_table + ` s WHERE s.user_id = $1 `

	_, e := c.db.Exec(ctx, q, id)

	return e
}
