package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) InsertAttends(ctx context.Context, attends *entity.Attend) (*entity.Attend, error) {
	e := exist{}
	u := `SELECT EXISTS (SELECT * FROM ` + Attend_table + ` WHERE user_id = $1)`

	_ = c.db.FetchRow(ctx, &e, u, attends.UserID)
	if e.Exists {
		return nil, fmt.Errorf("user already join meetings")
	}

	m := `SELECT EXISTS (SELECT * FROM ` + Meeting_table + ` WHERE id = $1)`
	_ = c.db.FetchRow(ctx, &e, m, attends.MeetingID)
	if e.Exists {
		q := `INSERT INTO ` + Attend_table + ` (id, user_id, meeting_id, role_id, created_at, updated_at, deleted_at) 
		VALUES ($1,$2,$3,$4,$5,$6,$7)`

		// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
		tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
			Isolation: sql.LevelRepeatableRead,
		})

		if err != nil {
			return nil, err
		}

		values := []interface{}{
			attends.ID,
			attends.UserID,
			attends.MeetingID,
			attends.RoleID,
			attends.CreatedAt,
			attends.UpdatedAt,
			attends.DeletedAt,
		}

		_, err = tx.Exec(q, values...)

		if err != nil {
			tx.Rollback()
			return nil, err
		}

		return attends, tx.Commit()
	}

	return nil, fmt.Errorf("meetings not exist")
}

func (c *client) UpdateAttends(ctx context.Context, meeting_id string, user_id uuid.UUID, part map[string]interface{}) (*entity.Attend, error) {
	word := ""
	block := 1
	args := make([]interface{}, 0)

	for i, v := range part {
		var chain string
		if block == len(part) {
			chain = fmt.Sprintf("%v = $%v", i, block)
		} else {
			chain = fmt.Sprintf("%v = $%v, ", i, block)
		}

		args = append(args, v)
		word += chain
		block++
	}

	filter := fmt.Sprintf("user_id = $%v AND meeting_id = $%v", block, block+1)
	args = append(args, user_id)
	args = append(args, meeting_id)

	q := `UPDATE ` + Attend_table + " SET " + word + ` WHERE ` + filter
	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return nil, err
	}

	_, err = tx.Exec(q, args...)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	tx.Commit()

	return c.GetUserAttend(ctx, meeting_id, user_id)
}

func (c *client) GetAttends(ctx context.Context, meeting_id string) ([]string, error) {
	attend := []string{}

	q := `SELECT a.access_id FROM ` + Attend_table + ` a WHERE a.meeting_id = $1`
	err := c.db.Fetch(ctx, &attend, q, meeting_id)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return attend, nil
}

func (c *client) GetUserAttend(ctx context.Context, meeting_id string, user_id uuid.UUID) (*entity.Attend, error) {
	attend := entity.Attend{}

	q := `SELECT * FROM ` + Attend_table + ` a WHERE a.meeting_id = $1 AND a.user_id = $2`
	err := c.db.FetchRow(ctx, &attend, q, meeting_id, user_id)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return &attend, nil
}

func (c *client) GetUserAndRoomInfo(ctx context.Context, meeting_id string, user_id uuid.UUID) (*entity.UserInRoomInfomation, error) {
	// SELECT a.id, m.access_id, u.username, r.name, a.created_at, a.updated_at, a.deleted_at FROM attends a INNER JOIN users u ON u.id = a.user_id INNER JOIN roles r ON r.id = a.role_id INNER JOIN meetings m ON m.id = a.meeting_id WHERE a.user_id = '2b9ccd10-14b1-4dca-b48f-2e1aa5dea817';
	info := entity.UserInRoomInfomation{}

	q := `SELECT a.id, a.meeting_id, a.user_id, u.username, r.name, a.created_at, a.updated_at, 
	a.deleted_at FROM ` + Attend_table + ` a INNER JOIN users u ON u.id = 
	a.user_id INNER JOIN ` + Role_table + ` r ON r.id = a.role_id WHERE a.user_id = $1 AND a.meeting_id = $2`

	err := c.db.FetchRow(ctx, &info, q, user_id, meeting_id)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return &info, nil
}

func (c *client) CheckAttends(ctx context.Context, user_id uuid.UUID) bool {
	e := exist{}
	u := `SELECT EXISTS (SELECT * FROM ` + Attend_table + ` WHERE user_id = $1)`

	_ = c.db.FetchRow(ctx, &e, u, user_id)
	return e.Exists
}

func (c *client) CheckHost(ctx context.Context, user_id uuid.UUID, role int) bool {
	u := `SELECT EXISTS (SELECT * FROM ` + Attend_table + ` WHERE user_id = $1 AND role = $2)`

	return c.checkQuery(ctx, u, user_id, role)
}

func (c *client) checkQuery(ctx context.Context, query string, valid ...interface{}) bool {
	e := exist{}
	_ = c.db.FetchRow(ctx, &e, query, valid)

	return e.Exists
}

func (c *client) DeleteAttends(ctx context.Context, meeting_id string, user_id uuid.UUID) error {
	q := ` DELETE FROM ` + Attend_table + ` a WHERE a.meeting_id = $1 AND a.user_id = $2`

	_, e := c.db.Exec(ctx, q, meeting_id, user_id)

	return e
}

func (c *client) DeleteAllAttends(ctx context.Context, meeting_id string, user_id uuid.UUID, role int) error {
	q := ` DELETE FROM ` + Attend_table + ` a WHERE a.meeting_id = $1 AND EXISTS(SELECT * FROM ` + Attend_table + ` WHERE user_id = $2 AND role = $3)`

	_, e := c.db.Exec(ctx, q, meeting_id, user_id, role)

	return e
}
