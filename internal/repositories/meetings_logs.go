package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) InsertMeetingsLogs(ctx context.Context, meeting *entity.MeetingLogs) error {
	q := `INSERT INTO ` + Meeting_logs_table + ` (id, user_id, meeting_id, meeting_name, created_at, updated_at, deleted_at) 
			VALUES ($1,$2,$3,$4,$5,$6,$7)`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return err
	}

	values := []interface{}{
		meeting.ID,
		meeting.UserID,
		meeting.MeetingID,
		meeting.MeetingName,
		meeting.CreatedAt,
		meeting.UpdatedAt,
		meeting.DeletedAt,
	}

	_, err = tx.Exec(q, values...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (c *client) UpdateMeetingsLogs(ctx context.Context, user_id uuid.UUID, meeting_id string, part map[string]interface{}) error {
	word := ""
	block := 1
	args := make([]interface{}, 0)

	for i, v := range part {
		var chain string
		if block == len(part) {
			chain = fmt.Sprintf("%v = $%v", i, block)
		} else {
			chain = fmt.Sprintf("%v = $%v, ", i, block)
		}

		args = append(args, v)
		word += chain
		block++
	}

	filter := fmt.Sprintf("user_id = $%v AND meeting_id = $%v", block, block+1)
	args = append(args, user_id, meeting_id)

	q := `UPDATE ` + Meeting_logs_table + " SET " + word + ` WHERE ` + filter
	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return err
	}

	_, err = tx.Exec(q, args...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (c *client) GetMeetingLogs(ctx context.Context, user_id uuid.UUID) ([]*entity.MeetingLogs, error) {
	q := `SELECT * FROM ` + Meeting_logs_table + ` m WHERE m.user_id = $1`

	session := []*entity.MeetingLogs{}

	err := c.db.Fetch(ctx, &session, q, user_id)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return session, nil
}
