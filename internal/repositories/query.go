package repositories

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/db"
)

const (
	User_table         = "users"
	Meeting_table      = "meetings"
	Meeting_logs_table = "meeting_logs"
	Session_table      = "sessions"
	Attend_table       = "attends"
	Role_table         = "roles"
	Token_table        = "tokens"
)

type exist struct {
	Exists bool `json:"exists" db:"exists"`
}

type Query interface {
	FindUser(ctx context.Context, id uuid.UUID, username, access_token string) (*entity.User, error)
	CatchUser(ctx context.Context, username, password string) (*entity.User, error)
	InsertUser(ctx context.Context, p entity.User) (*entity.User, error)
	GetUser(ctx context.Context, username string) (*entity.User, error)
	UpdateUser(ctx context.Context, id uuid.UUID, part map[string]interface{}) error

	InsertSession(ctx context.Context, param *entity.User) (*entity.Session, error)
	GetSession(ctx context.Context, user_id uuid.UUID) ([]*entity.Session, error)
	UpdateSession(ctx context.Context, id uuid.UUID) error
	InsertSessionAndTokens(ctx context.Context, param *entity.User, access_token string, exp time.Time) (*entity.Session, error)

	InsertMeetings(ctx context.Context, user_id uuid.UUID, meeting *entity.Meeting) (*entity.Meeting, error)
	DeleteMeetings(ctx context.Context, meeting_id string) error
	MeetingInfo(ctx context.Context, meeting_id string, room_name string) (map[string]interface{}, error)
	GetMeetings(ctx context.Context, meeting_id string, room_name string) ([]*entity.Meeting, error)
	UpdateMeetings(ctx context.Context, meeting_id string, part map[string]interface{}) error

	InsertAttends(ctx context.Context, attends *entity.Attend) (*entity.Attend, error)
	UpdateAttends(ctx context.Context, meeting_id string, user_id uuid.UUID, part map[string]interface{}) (*entity.Attend, error)
	GetAttends(ctx context.Context, meeting_id string) ([]string, error)
	GetUserAttend(ctx context.Context, meeting_id string, user_id uuid.UUID) (*entity.Attend, error)
	GetUserAndRoomInfo(ctx context.Context, meeting_id string, user_id uuid.UUID) (*entity.UserInRoomInfomation, error)
	DeleteAttends(ctx context.Context, meeting_id string, user_id uuid.UUID) error
	DeleteAllAttends(ctx context.Context, meeting_id string, user_id uuid.UUID, role int) error

	CheckHost(ctx context.Context, user_id uuid.UUID, role int) bool
	CheckAttends(ctx context.Context, user_id uuid.UUID) bool

	AllTokens(ctx context.Context) ([]entity.Tokens, error)
	DeleteSessionsAndTokens(ctx context.Context, target string, id uuid.UUID) error

	InsertMeetingsLogs(ctx context.Context, meeting *entity.MeetingLogs) error
	UpdateMeetingsLogs(ctx context.Context, user_id uuid.UUID, meeting_id string, part map[string]interface{}) error
	GetMeetingLogs(ctx context.Context, user_id uuid.UUID) ([]*entity.MeetingLogs, error)
}

type client struct {
	db     db.Adapter
	config appctx.Config
}

func NewClient(db db.Adapter, config appctx.Config) Query {
	return &client{db, config}
}
