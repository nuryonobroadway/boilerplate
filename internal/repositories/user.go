package repositories

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) FindUser(ctx context.Context, id uuid.UUID, username, access_token string) (*entity.User, error) {
	user := entity.User{}
	q := `SELECT * FROM ` + User_table + ` u WHERE username = $1 
		AND EXISTS (SELECT * FROM ` + Session_table + ` s WHERE s.user_id = u.id)
		AND EXISTS (SELECT * FROM ` + Token_table + ` t WHERE t.session_id = (
			SELECT s.id FROM ` + Session_table + ` s WHERE s.user_id = u.id))`

	err := c.db.FetchRow(ctx, &user, q, username)
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	logger.Info(user)
	if user.ID.String() == "" {
		return nil, fmt.Errorf("user not extracted")
	}

	return &user, nil
}

func (c *client) GetUser(ctx context.Context, username string) (*entity.User, error) {
	user := entity.User{}
	q := `SELECT * FROM ` + User_table + ` WHERE username = $1`

	err := c.db.FetchRow(ctx, &user, q, username)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if user.ID.String() == "" {
		return nil, fmt.Errorf("user not extracted")
	}

	return &user, nil
}

func (c *client) CatchUser(ctx context.Context, username, password string) (*entity.User, error) {
	user := entity.User{}

	// SELECT u.id, u.username, u.password FROM users u WHERE NOT EXISTS (SELECT * FROM sessions s WHERE s.user_id = u.id) AND username = 'adnan';
	q := `SELECT * FROM ` + User_table + ` u WHERE NOT EXISTS (SELECT * FROM ` + Session_table + ` s WHERE s.user_id = u.id) AND username = $1 `

	err := c.db.FetchRow(ctx, &user, q, username)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if user.ID.String() == "" {
		return nil, fmt.Errorf("user not extracted")
	}

	if !validator.ComparePassword(c.config.App.HexKey, user.Password, password) {
		return nil, fmt.Errorf("wrong password")
	}

	return &user, nil
}

func (c *client) InsertUser(ctx context.Context, param entity.User) (*entity.User, error) {
	e := exist{}
	// SELECT EXISTS (SELECT 1 FROM mytable WHERE col1 = 1);
	u := `SELECT EXISTS (SELECT * FROM ` + User_table + ` WHERE username = $1)`

	_ = c.db.FetchRow(ctx, &e, u, param.Username)
	if e.Exists {
		return nil, fmt.Errorf("user already exist")
	}

	param.ID = uuid.New()
	param.RoleID = 2
	param.CreatedAt = time.Now()
	param.UpdatedAt = time.Now()

	q := `INSERT INTO ` + User_table + ` (id, username, password, role_id, created_at, updated_at, deleted_at) 
			VALUES ($1,$2,$3,$4,$5,$6,$7)`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return nil, err
	}

	values := []interface{}{
		param.ID,
		param.Username,
		param.Password,
		param.RoleID,
		param.CreatedAt,
		param.UpdatedAt,
		param.DeletedAt,
	}

	_, err = tx.Exec(q, values...)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	tx.Commit()

	return &param, nil
}

func (c *client) UpdateUser(ctx context.Context, id uuid.UUID, part map[string]interface{}) error {
	word := ""
	block := 1
	args := make([]interface{}, 0)

	for i, v := range part {
		var chain string
		if block == len(part) {
			chain = fmt.Sprintf("%v = $%v", i, block)
		} else {
			chain = fmt.Sprintf("%v = $%v, ", i, block)
		}

		args = append(args, v)
		word += chain
		block++
	}

	filter := fmt.Sprintf("ID = $%v", block)
	args = append(args, id)

	q := `UPDATE ` + User_table + " SET " + word + ` WHERE ` + filter

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return err
	}

	_, err = tx.Exec(q, args...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}
