package repositories

import (
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/db"
)

type storer struct {
	dbTx db.Transaction
}

// NewStore create ne instance database transaction
func NewStore(dbTx db.Transaction) *storer {
	return &storer{dbTx: dbTx}
}
