package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) InsertMeetings(ctx context.Context, user_id uuid.UUID, meeting *entity.Meeting) (*entity.Meeting, error) {
	// SELECT EXISTS (SELECT * FROM attends WHERE user_id = '2b9ccd10-14b1-4dca-b48f-2e1aa5dea817')
	e := exist{}
	u := `SELECT EXISTS (SELECT * FROM ` + Attend_table + ` WHERE user_id = $1)`

	_ = c.db.FetchRow(ctx, &e, u, user_id)
	if e.Exists {
		return nil, fmt.Errorf("user already join meetings")
	}

	q := `INSERT INTO ` + Meeting_table + ` (id, access_id, room_name, room_password, created_at, updated_at, deleted_at) 
			VALUES ($1,$2,$3,$4,$5,$6,$7)`

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return nil, err
	}

	values := []interface{}{
		meeting.ID,
		meeting.AccessID,
		meeting.RoomName,
		meeting.RoomPassword,
		meeting.CreatedAt,
		meeting.UpdatedAt,
		meeting.DeletedAt,
	}

	_, err = tx.Exec(q, values...)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	return meeting, tx.Commit()
}

func (c *client) QueryMeetings(ctx context.Context, query string, args ...interface{}) ([]*entity.Meeting, error) {
	session := []*entity.Meeting{}

	err := c.db.Fetch(ctx, &session, query, args...)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return session, nil
}

func (c *client) GetMeetings(ctx context.Context, meeting_id string, room_name string) ([]*entity.Meeting, error) {
	q := `SELECT * FROM ` + Meeting_table + ` m WHERE m.id = $1 OR m.room_name = $2`
	return c.QueryMeetings(ctx, q, meeting_id, room_name)
}

func (c *client) DeleteMeetings(ctx context.Context, meeting_id string) error {
	q := ` DELETE FROM ` + Meeting_table + ` m WHERE m.id = $1`

	_, e := c.db.Exec(ctx, q, meeting_id)

	return e
}

func (c *client) UpdateMeetings(ctx context.Context, meeting_id string, part map[string]interface{}) error {
	word := ""
	block := 1
	args := make([]interface{}, 0)

	for i, v := range part {
		var chain string
		if block == len(part) {
			chain = fmt.Sprintf("%v = $%v", i, block)
		} else {
			chain = fmt.Sprintf("%v = $%v, ", i, block)
		}

		args = append(args, v)
		word += chain
		block++
	}

	filter := fmt.Sprintf("id = $%v", block)
	args = append(args, meeting_id)

	q := `UPDATE ` + Meeting_table + " SET " + word + ` WHERE ` + filter
	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	tx, err := c.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelRepeatableRead,
	})

	if err != nil {
		return err
	}

	_, err = tx.Exec(q, args...)

	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (c *client) MeetingInfo(ctx context.Context, meeting_id string, name string) (map[string]interface{}, error) {
	e := exist{}
	u := `SELECT EXISTS (SELECT * FROM ` + Meeting_table + ` WHERE id = $1)`

	_ = c.db.FetchRow(ctx, &e, u, meeting_id)
	if e.Exists {
		meetings, err := c.GetMeetings(ctx, meeting_id, name)
		if err != nil {
			logger.Error(err)
			return nil, err
		}

		attends, err := c.GetAttends(ctx, meeting_id)
		if err != nil {
			logger.Error(err)
			return nil, err
		}

		return map[string]interface{}{
			"meetings": meetings,
			"attends":  attends,
		}, nil
	}

	return nil, fmt.Errorf("meetings not found")
}
