package repositories

import (
	"context"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func (c *client) AllTokens(ctx context.Context) ([]entity.Tokens, error) {
	tokens := []entity.Tokens{}

	q := `SELECT * FROM ` + Token_table
	err := c.db.Fetch(ctx, &tokens, q)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return tokens, nil
}

func (c *client) DeleteSessionsAndTokens(ctx context.Context, target string, id uuid.UUID) error {
	/*
		WITH resultID(id) as (DELETE FROM tokens t WHERE t.session_id = (SELECT s.id FROM sessions s WHERE s.id = 'fa2185e4-fd76-425c-98fe-389424991a3f') RETURNING t.session_id)
		DELETE FROM sessions s WHERE s.id = (SELECT resultID.id FROM resultID)
	*/

	q := `WITH resultID(id) as (DELETE FROM ` + Token_table + ` t WHERE t.session_id = (SELECT s.id FROM `+ Session_table +` s WHERE s.`+ target +` = $1) RETURNING t.session_id) 
		DELETE FROM ` + Session_table + ` s WHERE s.id = (SELECT resultID.id FROM resultID) `

	_, err := c.db.Exec(ctx, q, id)
	if err != nil {
		return err
	}

	return nil
}
