package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

const (
	authorizationTypeBearer = "bearer"
	authorizationPayloadKey = "authorization_payload"
)

func ValidateToken(r *http.Request, conf *appctx.Config) int {
	authorizationHeader := r.Header.Get(httpclient.Authorization)
	if len(authorizationHeader) == 0 {
		logger.Error("error: authorization header is not provided")
		return consts.CodeAuthenticationFailure
	}

	fields := strings.Fields(authorizationHeader)
	if len(fields) < 2 {
		logger.Error("invalid authorization header format")
		return consts.CodeAuthenticationFailure
	}

	authorizationType := strings.ToLower(fields[0])
	if authorizationType != authorizationTypeBearer {
		err := fmt.Errorf("unsupported authorization type %s", authorizationType)
		logger.Error(err)
		return consts.CodeAuthenticationFailure
	}

	token, err := helper.VerifyToken(conf, fields[1])
	if err != nil {
		logger.Error(err)
		return consts.CodeAuthenticationFailure
	}

	if !token.Valid {
		logger.Error(fmt.Errorf("error: jwt not valid"))
		return consts.CodeAuthenticationFailure
	}

	return consts.CodeSuccess
}
