// Package middleware
package middleware

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net/http"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

// // ValidateSignature header signature
// func ValidateSignature(r *http.Request, conf *appctx.Config) int {

// 	var err error
// 	key := conf.App.ApiKey

// 	signature := r.Header.Get("Signature")

// 	save := r.Body

// 	save, r.Body, err = drainBody(r.Body)

// 	nameField := "request"

// 	if err != nil {
// 		return consts.CodeBadRequest
// 	}

// 	b, err := io.ReadAll(r.Body)

// 	if err != nil {
// 		logger.Warn(fmt.Sprintf("[middleware] cannot read request body , error %s", err.Error()), logger.Any(nameField, string(b)))
// 		return consts.CodeBadRequest
// 	}

// 	buff := &bytes.Buffer{}
// 	err = json.Compact(buff, b)
// 	if err != nil {
// 		logger.Error(fmt.Sprintf("[middleware] cannot compat bytes , error %s", err.Error()), logger.Any(nameField, string(b)))
// 		return consts.CodeBadRequest
// 	}

// 	if !hash.HmacComparator(buff.String(), signature, hash.SHA256(key)) {
// 		logger.Warn(fmt.Sprintf("[middleware] invalid signature %s", signature), logger.Any(nameField, string(b)))
// 		return consts.CodeBadRequest
// 	}

// 	r.Body = save

// 	return consts.CodeSuccess
// }

// func drainBody(b io.ReadCloser) (r1, r2 io.ReadCloser, err error) {
// 	if b == http.NoBody {
// 		// No copying needed. Preserve the magic sentinel meaning of NoBody.
// 		return http.NoBody, http.NoBody, nil
// 	}

// 	var buf bytes.Buffer
// 	if _, err = buf.ReadFrom(b); err != nil {
// 		return nil, b, err
// 	}
// 	if err = b.Close(); err != nil {
// 		return nil, b, err
// 	}
// 	return io.NopCloser(&buf), io.NopCloser(bytes.NewReader(buf.Bytes())), nil
// }

func ValidateSignature(r *http.Request, conf *appctx.Config) int {
	clientSignature := r.Header.Get("Signature")
	clientKeyID := r.Header.Get("x_api_key_id")
	clientTimestamp := r.Header.Get("Timestamp")

	if clientSignature == "" {
		logger.Error("signature not reconized")
		return consts.CodeBadRequest
	}

	if clientTimestamp == "" {
		logger.Error("timestamp not reconized")
		return consts.CodeBadRequest
	}

	if clientKeyID == "" {
		logger.Error("x_api_key_id not reconized")
		return consts.CodeBadRequest
	}

	hmac_signature := fmt.Sprintf("%v:%v:%v", clientTimestamp, clientKeyID, r.Method)
	secret := conf.App.ApiKey

	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(hmac_signature))

	signature := fmt.Sprintf("#%v:#%v", clientKeyID, base64.StdEncoding.EncodeToString(h.Sum(nil)))
	serverSignature := base64.StdEncoding.EncodeToString([]byte(signature))

	if !hmac.Equal([]byte(clientSignature), []byte(serverSignature)) {
		return consts.CodeBadRequest
	}

	return consts.CodeSuccess
}
