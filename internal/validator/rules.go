// Package validator
package validator

import (
	"fmt"
	"net/http"
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/thedevsaddam/govalidator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/util"
)

// ValidateAlphaNumericDash for reference transaction id
func ValidateAlphaNumericDash(v string) bool {
	pattern := `^[0-9a-zA-Z\-]+$`

	rgx, err := regexp.Compile(pattern)

	if err != nil {
		return false
	}

	return rgx.MatchString(v)
}

func AlphaNumericDash() validation.StringRule {
	return validation.NewStringRuleWithError(
		ValidateAlphaNumericDash,
		validation.NewError("validation_is_alphanumeric", "must contain alpha, digits and dash only"))
}

func IsValidUUID(u string) (uuid.UUID, error, bool) {
	id, err := uuid.Parse(u)
	if err != nil {
		return uuid.UUID{}, err, false
	}

	return id, nil, true
}

func ComparePassword(key string, decrypt_password, password string) bool {
	return helper.DecryptText(key, decrypt_password) == password
}

func ParameterIsExist(r *http.Request, block int, parameter []string, target ...string) ([]string, error) {
	if block == len(target) {
		return parameter, nil
	} else {
		source := mux.Vars(r)[target[block]]
		if source == "" {
			return parameter, fmt.Errorf("parameter {%v} not Exists", target[block])
		}

		parameter = append(parameter, source)
		return ParameterIsExist(r, block+1, parameter, target...)
	}
}

func FormValidator(d *appctx.Data, e interface{}, valid ...string) (interface{}, error) {
	err := d.Cast(&e)

	if err != nil {
		logger.Error(logger.MessageFormat("[example-create] parsing body request error: %v", err))
		return nil, err
	}

	fl := []logger.Field{
		logger.Any("request", e),
	}

	rules := govalidator.MapData{}
	for _, v := range valid {
		rules[v] = []string{"required"}
	}

	opts := govalidator.Options{
		Data:  &e,    // request object
		Rules: rules, // rules map
	}

	v := govalidator.New(opts)
	ev := v.ValidateStruct()

	if len(ev) != 0 {
		logger.Warn(
			logger.MessageFormat("[example-create] validate request param err: %s", util.DumpToString(ev)),
			fl...)

		return nil, fmt.Errorf("[example-create] validate request param err: %s", util.DumpToString(ev))
	}

	return e, nil
}
