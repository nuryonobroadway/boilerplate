package validator_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
)

func TestParameterIsExist(t *testing.T) {

}

func TestRequestParameter(t *testing.T) {
	// Create a request with the parameter
	req, err := http.NewRequest("GET", "/?param=123", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a response recorder
	rr := httptest.NewRecorder()

	// Call the handler function
	handler(rr, req)

	// Check the status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// Check the response body
	expected := "param=123"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestRequestPathVariable(t *testing.T) {
	// Create a router
	r := mux.NewRouter()

	// Register the route with a path variable
	r.HandleFunc("/users/{username}/{number}", handler).Methods("GET")

	// Create a request with the path variable
	req, err := http.NewRequest("GET", "/users/adnan/123", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a response recorder
	rr := httptest.NewRecorder()

	// Call the handler function
	r.ServeHTTP(rr, req)

	// Check the status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

}

func handler(w http.ResponseWriter, r *http.Request) {
	testCases := []struct {
		name      string
		parameter []string
		buidup    func(r *http.Request, builder []string)
	}{
		{
			name:      "ok",
			parameter: []string{"username", "number"},
			buidup: func(r *http.Request, builder []string) {
				result, _ := validator.ParameterIsExist(r, 0, []string{}, builder...)
				expected := []string{"adnan", "123"}
				same := true
				if len(result) != len(expected) {
					same = false
				} else {
					for i := range result {
						if result[i] != expected[i] {
							same = false
							break
						}
					}
				}

				// Print the result
				if same {
					fmt.Println("The arrays are the same.")
				} else {
					fmt.Println("The arrays are not the same.")
				}
			},
		}, {
			name:      "not found in path",
			parameter: []string{"username", "number", "test"},
			buidup: func(r *http.Request, builder []string) {
				result, err := validator.ParameterIsExist(r, 0, []string{}, builder...)
				expected := []string{"adnan", "123"}
				error_expected := "parameter {test} not Exists"
				if err.Error() == error_expected {
					fmt.Println("yes it is")
				}

				if len(result) == len(expected) {
					if result[0] == expected[0] {
						fmt.Println("yes it is")
					}
				}
			},
		}, {
			name:      "return empty parameter",
			parameter: []string{},
			buidup: func(r *http.Request, builder []string) {
				result, _ := validator.ParameterIsExist(r, 0, []string{}, builder...)
				expected := []string{}

				if len(result) == len(expected) {
					fmt.Println("yes it is")
				}
			},
		},
	}

	for _, v := range testCases {
		fmt.Println("=======================")
		fmt.Println(v.name)
		parameter := v.parameter
		v.buidup(r, parameter)
	}

}

// func handler(w http.ResponseWriter, r *http.Request) {
// 	// Get the request parameter
// 	param := r.URL.Query().Get("param")
// 	w.Write([]byte("param=" + param))
// }
