package usersucase

import (
	"strings"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
)

type userProfile struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewUserProfile(config *appctx.Config, repo repositories.Query) *userProfile {
	return &userProfile{config, repo}
}

func (t *userProfile) Serve(d *appctx.Data) appctx.Response {
	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err.Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	user, err := t.repo.FindUser(d.Request.Context(), id, claims.Username, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeNotFound).
			WithError(err.Error())
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	response := map[string]interface{}{
		"id":         user.ID,
		"username":   user.Username,
		"created_at": user.CreatedAt,
		"updated_at": user.UpdatedAt,
		"deleted_at": user.DeletedAt,
	}

	return *appctx.NewResponse().
		WithCode(consts.CodeSuccess).
		WithMeta(meta).
		WithData(response)
}
