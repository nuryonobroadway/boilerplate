package usersucase

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type userRegister struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewUserRegister(config *appctx.Config, repo repositories.Query) *userRegister {
	return &userRegister{config, repo}
}

func (t *userRegister) Serve(d *appctx.Data) appctx.Response {
	user := entity.User{}

	data, err := io.ReadAll(d.Request.Body)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithMessage(err.Error())
	}

	if err := json.Unmarshal(data, &user); err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithMessage(err.Error())
	}

	if user.Username == "" {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithMessage(fmt.Errorf("error user is nil").Error())
	}

	// valid := []string{
	// 	"username",
	// 	"password",
	// }
	// u, err := validator.FormValidator(d, entity.User{}, valid...)
	// if err != nil {
	// 	return *appctx.NewResponse().WithCode(consts.CodeBadRequest).WithError(err)
	// }

	// user := u.(entity.User)
	user.Password = helper.EncryptText(t.config.App.HexKey, user.Password)
	result, err := t.repo.InsertUser(d.Request.Context(), user)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithMessage(err.Error())
	}
	meta := map[string]interface{}{
		"id": result.ID,
	}

	response := map[string]interface{}{
		"username":   result.Username,
		"created_at": result.CreatedAt,
		"updated_at": result.UpdatedAt,
	}

	return *appctx.NewResponse().
		WithCode(consts.CodeSuccess).
		WithMeta(meta).
		WithData(response)
}
