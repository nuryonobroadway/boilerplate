package usersucase

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type userChangePassword struct {
	config *appctx.Config
	repo   repositories.Query
}

type changePasswordRequired struct {
	OldPassword  string `json:"old_password"`
	NewPassword  string `json:"new_password"`
	ConfPassword string `json:"conf_password"`
}

func NewUserChangePassword(config *appctx.Config, repo repositories.Query) *userChangePassword {
	return &userChangePassword{config, repo}
}

func (t *userChangePassword) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	username := path[0]
	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err)
	}

	if claims.Username != username {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(fmt.Errorf("username not same"))
	}

	passwordField := changePasswordRequired{}
	data, err := io.ReadAll(d.Request.Body)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithError(err)
	}

	if err := json.Unmarshal(data, &passwordField); err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithError(err)
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil && passwordField.NewPassword != passwordField.ConfPassword {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err)
	}

	user, err := t.repo.FindUser(d.Request.Context(), id, claims.Username, token)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeNotFound).
			WithError(err)
	}

	if !validator.ComparePassword(t.config.App.HexKey, user.Password, passwordField.OldPassword) {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err)
	}

	changed := map[string]interface{}{
		"password": helper.EncryptText(t.config.App.HexKey, passwordField.NewPassword),
	}

	if err := t.repo.UpdateUser(d.Request.Context(), user.ID, changed); err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err)
	}

	return *appctx.NewResponse().
		WithCode(consts.CodeSuccess)
}
