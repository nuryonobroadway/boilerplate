package usersucase

import (
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type userSession struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewUserSession(config *appctx.Config, repo repositories.Query) *userSession {
	return &userSession{config, repo}
}

func (t *userSession) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithMessage(err.Error())
	}

	username := path[0]
	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithMessage(err.Error())
	}

	if claims.Username != username {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithMessage(err.Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithMessage(err.Error())
	}

	sessions, err := t.repo.GetSession(d.Request.Context(), id)
	if err != nil {
		messages := map[string]interface{}{
			"report": "user already logout",
		}

		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithMessage(err.Error()).
			WithData(messages)
	}

	return *appctx.NewResponse().
		WithCode(consts.CodeSuccess).
		WithData(sessions)
}
