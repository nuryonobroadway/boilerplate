package usersucase

import (
	"fmt"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type userLogout struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewUserLogout(config *appctx.Config, repo repositories.Query) *userLogout {
	return &userLogout{config, repo}
}

func (t *userLogout) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err.Error())
	}

	username := path[0]
	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err.Error())
	}

	if claims.Username != username {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(fmt.Errorf("username not same").Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	err = t.repo.DeleteSessionsAndTokens(d.Request.Context(), "user_id", id)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	message := map[string]interface{}{
		"response": "success logout user",
	}

	return *appctx.NewResponse().
		WithCode(consts.CodeSuccess).
		WithData(message)
}
