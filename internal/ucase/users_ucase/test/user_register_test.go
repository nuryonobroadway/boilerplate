package test_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	mockdb "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories/mock"
	clienttest "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/test"
)

func TestUserRegister(t *testing.T) {
	testCases := []struct {
		name          string
		body          []byte
		buildStubs    func(mock *mockdb.MockQuery)
		checkResponse func(t *testing.T, recorder *httptest.ResponseRecorder)
	}{
		{
			name: "ok",
			body: []byte(`{"username": "adnan", "password": "123"}`),
			buildStubs: func(mock *mockdb.MockQuery) {
				req := entity.User{
					Username: "adnan",
					Password: "123",
				}

				res := entity.User{
					ID:        uuid.New(),
					Username:  "adnan",
					Password:  "123",
					RoleID:    4,
					CreatedAt: time.Now(),
					UpdatedAt: time.Now(),
					DeletedAt: time.Time{},
				}

				mock.EXPECT().InsertUser(gomock.Any(), gomock.Eq(req)).
					Times(1).
					Return(res, nil)
			},
			checkResponse: func(t *testing.T, recorder *httptest.ResponseRecorder) {
				_, err := io.ReadAll(recorder.Body)
				require.NoError(t, err)
				require.Equal(t, recorder.Code, consts.CodeSuccess)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]

		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mockdb.NewMockQuery(ctrl)
			server, err := clienttest.NewTestServer(store)
			require.NoError(t, err)

			server.Run(ctx)

			router := clienttest.NewRoute(server.Config(), store)

			data, err := json.Marshal(tc.body)
			require.NoError(t, err)

			recorder := httptest.NewRecorder()
			url := "http://0.0.0.0:8000/v1/auth/register"
			request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(data))
			require.NoError(t, err)

			router.Route().ServeHTTP(recorder, request)

			tc.checkResponse(t, recorder)

		})
	}
}
