package usersucase

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type userLogin struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewUserLogin(config *appctx.Config, repo repositories.Query) *userLogin {
	return &userLogin{config, repo}
}

func (t *userLogin) Serve(d *appctx.Data) appctx.Response {
	user := entity.User{}
	data, err := io.ReadAll(d.Request.Body)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithError(err)
	}

	if err := json.Unmarshal(data, &user); err != nil {
		logger.Error(err)

		return *appctx.NewResponse().
			WithCode(http.StatusBadRequest).
			WithError(err.Error())
	}
	if user.Username == "" && user.Password == "" {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(fmt.Errorf("error user is nil").Error())
	}

	result, err := t.repo.CatchUser(d.Request.Context(), user.Username, user.Password)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err.Error())
	}

	exp := time.Now().Add(time.Hour * 2)
	token, err := helper.GenerateToken(t.config, exp, result)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	// InsertSessionAndTokens(ctx context.Context, param *entity.User, access_token string) (*entity.Session, error)
	session, err := t.repo.InsertSessionAndTokens(d.Request.Context(), result, token, exp)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	response := map[string]interface{}{
		"session_id": session.ID,
		"type":       "bearer",
		"token":      token,
		"expired_at": exp,
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMeta(meta).
		WithData(response)
}
