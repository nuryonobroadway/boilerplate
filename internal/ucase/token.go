package ucase

import (
	"net/http"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
)

type token struct {
	config *appctx.Config
}

func NewToken(config *appctx.Config) *token {
	return &token{config}
}

func (t *token) Serve(data *appctx.Data) appctx.Response {
	exp := time.Now().Add(time.Hour * 2)
	newJwt := jwt.New(jwt.SigningMethodHS256)
	claims := newJwt.Claims.(jwt.MapClaims)
	claims["exp"] = exp.Unix()
	token, err := newJwt.SignedString([]byte(t.config.App.ApiKey))
	if err != nil {
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	meta := map[string]interface{}{
		"transaction_id": uuid.New(),
	}

	response := map[string]interface{}{
		"type":       "bearer",
		"token":      token,
		"expired_at": exp,
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithMeta(meta).
		WithData(response)
}
