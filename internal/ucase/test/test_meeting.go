package clienttest

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"runtime/debug"
	"time"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/bootstrap"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/handler"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/router"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/server"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/routerkit"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
	usersucase "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/users_ucase"
)

// NewHTTPServer creates http server instance
// returns: Server instance
func NewTestServer(store repositories.Query) (server.Server, error) {

	cfg, err := appctx.NewConfig()
	if err != nil {
		// logger.Fatal(fmt.Sprintf("Load config error %v", err), logger.EventName("InitiateConfig"))
		return nil, err
	}

	return &httpServer{
		config: cfg,
		router: NewRoute(cfg, store),
	}, nil
}

// httpServer as HTTP server implementation
type httpServer struct {
	config *appctx.Config
	router router.Router
}

// Run runs the http server gracefully
// returns:
//
//	err: error operation
func (h *httpServer) Run(ctx context.Context) error {
	var err error

	server := http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%d", h.config.App.Port),
		Handler:      h.router.Route(),
		ReadTimeout:  time.Duration(h.config.App.ReadTimeoutSecond) * time.Second,
		WriteTimeout: time.Duration(h.config.App.WriteTimeoutSecond) * time.Second,
	}

	go func() {
		err = server.ListenAndServe()
		if err != http.ErrServerClosed {
			logger.Error(logger.MessageFormat("http server got %v", err), logger.EventName(consts.LogEventNameServiceStarting))
		}
	}()

	<-ctx.Done()

	ctxShutDown, cancel := context.WithTimeout(context.Background(), 6*time.Second)
	defer func() {
		cancel()
	}()

	if err = server.Shutdown(ctxShutDown); err != nil {
		logger.Fatal(logger.MessageFormat("server Shutdown Failed:%v", err), logger.EventName(consts.LogEventNameServiceTerminated))
	}

	logger.Info("server exited properly", logger.EventName(consts.LogEventNameServiceTerminated))

	if err == http.ErrServerClosed {
		err = nil
	}

	return err
}

// Done runs event wheen service stopped
func (h *httpServer) Done() {
	logger.Info("service http stopped", logger.EventName(consts.LogEventNameServiceTerminated))
}

// Config  func to handle get config will return Config object
func (h *httpServer) Config() *appctx.Config {
	return h.config
}

type r struct {
	config *appctx.Config
	router *routerkit.Router
	mock   repositories.Query
}

func NewRoute(cfg *appctx.Config, mock repositories.Query) router.Router {
	return &r{
		config: cfg,
		router: routerkit.NewRouter(routerkit.WithServiceName(cfg.App.AppName)),
		mock:   mock,
	}
}

// Route preparing http router and will return mux router object
func (rtr *r) Route() *routerkit.Router {

	root := rtr.router.PathPrefix("/").Subrouter()
	//in := root.PathPrefix("/in/").Subrouter()
	// liveness := root.PathPrefix("/").Subrouter()
	//inV1 := in.PathPrefix("/v1/").Subrouter()
	in := root.PathPrefix("/v1/auth").Subrouter()

	// open tracer setup
	bootstrap.RegistryOpenTracing(rtr.config)

	// db := bootstrap.RegistryMariaMasterSlave(rtr.config.WriteDB, rtr.config.ReadDB)
	// db := bootstrap.RegistryPostgreDB(rtr.config.ReadDB, rtr.config.App.Timezone)
	// user := repositories.NewClient(rtr.mock, *rtr.config)
	// use case

	// healthy
	in.HandleFunc("/register", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserRegister(rtr.config, rtr.mock),
		middleware.ValidateSignature,
	)).Methods(http.MethodPost)

	in.HandleFunc("/login", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserLogin(rtr.config, rtr.mock),
		middleware.ValidateSignature,
	)).Methods(http.MethodPost)

	in.HandleFunc("/logout/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserLogout(rtr.config, rtr.mock),
		middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/profile/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserProfile(rtr.config, rtr.mock),
		middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/sessions/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserSession(rtr.config, rtr.mock),
		middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/change_password/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserChangePassword(rtr.config, rtr.mock),
		middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodPatch)

	return rtr.router

}

type httpHandlerFunc func(request *http.Request, svc contract.UseCase, conf *appctx.Config) appctx.Response

func (rtr *r) handle(hfn httpHandlerFunc, svc contract.UseCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				w.WriteHeader(http.StatusInternalServerError)
				res := appctx.Response{
					Code: consts.CodeInternalServerError,
				}

				res.GenerateMessage()
				logger.Error(logger.MessageFormat("error %v", string(debug.Stack())))
				json.NewEncoder(w).Encode(res)
				return
			}
		}()

		ctx := context.WithValue(r.Context(), "access", map[string]interface{}{
			"path":      r.URL.Path,
			"remote_ip": r.RemoteAddr,
			"method":    r.Method,
		})

		req := r.WithContext(ctx)

		if status := middleware.FilterFunc(rtr.config, req, mdws); status != 200 {
			rtr.response(w, appctx.Response{
				Code: status,
			})

			return
		}

		resp := hfn(req, svc, rtr.config)
		resp.Lang = rtr.defaultLang(req.Header.Get(consts.HeaderLanguageKey))
		rtr.response(w, resp)
	}
}

func (rtr *r) response(w http.ResponseWriter, resp appctx.Response) {

	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)

	defer func() {
		resp.GenerateMessage()
		w.WriteHeader(resp.GetCode())
		json.NewEncoder(w).Encode(resp)
	}()

	return

}

func (rtr *r) defaultLang(l string) string {

	if len(l) == 0 {
		return rtr.config.App.DefaultLang
	}

	return l
}
