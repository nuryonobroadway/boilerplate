package meetingsucase

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type ClientList map[*Client]bool
type RoomList map[string][]string

type Client struct {
	connection *websocket.Conn
	manager    *meetingJoin
	egress     chan Event
	user       *entity.Attend
}

var (
	pongWait = 60 * time.Second
	// ping interval harus di set kurang dari pongWait, untuk mendapatkan 90% dari waktu gunakan 0 * 9 / 10
	// alasanya adalah pingInterval harus lebih rendah dari pingRequecnty, dikarenakan nantinya akan send ping baru sebelum mendapatkan response
	pingInterval = (pongWait * 9) / 10
)

func NewClient(conn *websocket.Conn, manager *meetingJoin, user *entity.Attend) *Client {
	return &Client{
		connection: conn,
		manager:    manager,
		egress:     make(chan Event),
		user:       user,
	}
}

// handle message jalankan dengan goroutine
func (c *Client) readMessages() {
	defer func() {
		// remove client jika goroutine terstop
		c.manager.removeClient(c)
	}()
	// set maksimal message 512 byte
	c.connection.SetReadLimit(512)
	// konfigurasi waktu tunggu untuk read pong response, jika sudah waktu deadline maka akan return error
	if err := c.connection.SetReadDeadline(time.Now().Add(pongWait)); err != nil {
		logger.Error(err)
		return
	}

	// konfigurasi untuk set handler untuk pong
	c.connection.SetPongHandler(c.pongHandler)

	// infinite loop
	for {
		// read message
		_, payload, err := c.connection.ReadMessage()
		if err != nil {
			// jika koneksi error, maka akan ditampilkan disini
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure, websocket.CloseNoStatusReceived) {
				logger.Error("error reading message: " + err.Error())
			}
			break // break loop dan tutup koneksi websocket
		}

		var request Event
		if err := json.Unmarshal(payload, &request); err != nil {
			logger.Error("error marshalling message: " + err.Error())
			break // Breaking the connection here might be harsh xD
		}

		// Route the Event
		if err := c.manager.routeEvent(request, c); err != nil {
			logger.Error("Error handeling Message: " + err.Error())
			break
		}
	}
}

// handle pong message
func (c *Client) pongHandler(pongMsg string) error {
	logger.Info(fmt.Sprintf("%v: pong %v", c.user.UserID, pongMsg))

	// reset timer pada pong message read deadline
	return c.connection.SetReadDeadline(time.Now().Add(pongWait))
}

func (c *Client) writeMessages() {
	ticker := time.NewTicker(pingInterval)
	defer func() {
		c.manager.removeClient(c)
	}()

	for {
		select {

		// blocking statement
		case message, ok := <-c.egress:
			// jika engress ditutup maka akan return false pada ok
			if !ok {
				// koneksi ditutup dan dikomunikasikan ke frontend
				if err := c.connection.WriteMessage(websocket.CloseMessage, nil); err != nil {
					// log error alasan apa yang menyebabkan koneksi terputus
					logger.Error("connection closed: " + err.Error())
				}
				// close goroutine
				return
			}

			data, err := json.Marshal(message)
			if err != nil {
				logger.Error(err)
				return
			}
			// menulis reqular message dan dikirimkan ke server
			if err := c.connection.WriteMessage(websocket.TextMessage, data); err != nil {
				logger.Error(err)
				return
			}
		case <-ticker.C:
			// mengirim ping berdasarkan tick time
			if err := c.connection.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				logger.Info("writemsg: " + err.Error())
				return // jika error break goroutine
			}
		}

	}
}
