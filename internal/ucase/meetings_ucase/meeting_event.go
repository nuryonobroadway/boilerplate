package meetingsucase

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type Event struct {
	// Type is the message type sent
	Type string `json:"type"`
	// Payload is the data Based on the Type
	Payload json.RawMessage `json:"payload"`
}

const (
	host   = "host"
	viewer = "viewer"
)

type broadcastMsg struct {
	RoomID     string   `json:"room_id"`
	AccessID   string   `json:"access_id"`
	ListClient []string `json:"list_client"`
}

type broadcastMessage struct {
	Username string `json:"username"`
	Message  string `json:"message"`
}

type NewbroadcastMessage struct {
	broadcastMessage
	Sent time.Time `json:"sent"`
}

type broadcastNewMsg struct {
	broadcastMsg
	Sent time.Time `json:"sent"`
}

type someoneDisconnected struct {
	Role   string `json:"role"`
	RoomID string `json:"room_id"`
	UserID string `json:"user_id"`
}

type NewSomeoneDisconnedted struct {
	UserID string `json:"user_id"`
}

type hostDestroyedCall struct {
	Destroyed bool `json:"destroyed"`
}

type newHostDestroyedCall struct {
	hostDestroyedCall
	Sent time.Time `json:"sent"`
}

type EventHandler func(event Event, c *Client) error

const (
	EventSendMessage       = "send_message"
	EventSomeOneJoin       = "user_connected"
	EventSomeOneOut        = "user_disconnected"
	EventClientList        = "client_list"
	EventCallUser          = "user_call"
	EventHostDestroyedCall = "host_destroyed_call"
	EventCleanUpMeetings   = "host_clean_up_meetings"

	EventNewMessage  = "new_message"
	EventHostConnect = "host_connect"
)

func HostDestroyedCall(event Event, c *Client) error {
	var hostDestroyed hostDestroyedCall
	if err := json.Unmarshal(event.Payload, &hostDestroyed); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	var newMessage newHostDestroyedCall
	newMessage.Destroyed = hostDestroyed.Destroyed
	newMessage.Sent = time.Now()

	data, err := json.Marshal(newMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	// Place payload into an Event
	var outgoingEvent Event
	outgoingEvent.Payload = data
	outgoingEvent.Type = EventHostDestroyedCall

	for client := range c.manager.clients {
		// Only send to clients inside the same chatroom
		if client.user.MeetingID == c.user.MeetingID {
			if client.connection != c.connection {
				client.egress <- outgoingEvent
			}
		}

	}

	return nil
}

func SendMessageHandler(event Event, c *Client) error {
	// Marshal Payload into wanted format
	var chatevent broadcastMessage
	if err := json.Unmarshal(event.Payload, &chatevent); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	var broadMessage NewbroadcastMessage

	broadMessage.Sent = time.Now()
	broadMessage.Username = chatevent.Username
	broadMessage.Message = chatevent.Message

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	var outgoingEvent Event
	outgoingEvent.Payload = data
	outgoingEvent.Type = EventNewMessage

	for client := range c.manager.clients {
		if client.user.MeetingID == c.user.MeetingID {
			client.egress <- outgoingEvent
		}

	}

	return nil
}

func HostConnected(event Event, c *Client) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var chatevent broadcastMsg
	if err := json.Unmarshal(event.Payload, &chatevent); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	value := map[string]interface{}{
		"access_id": chatevent.AccessID,
	}

	err := c.manager.repo.UpdateMeetings(ctx, chatevent.RoomID, value)
	if err != nil {
		return fmt.Errorf("rooms not found: %v", err)
	}

	list, err := c.manager.repo.GetAttends(ctx, chatevent.RoomID)
	if err != nil {
		return fmt.Errorf("rooms not found: %v", err)
	}

	outgoingEvent, err := preparedDataToSend(c, list, chatevent.RoomID, chatevent.AccessID, EventHostConnect)
	if err != nil {
		return fmt.Errorf("error when prepared data: %v", err)
	}

	for client := range c.manager.clients {
		if client.user.MeetingID == c.user.MeetingID {
			if client.connection == c.connection {
				client.egress <- outgoingEvent
			}
		}

	}

	return nil
}

func SomeoneJoin(event Event, c *Client) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var chatevent broadcastMsg
	if err := json.Unmarshal(event.Payload, &chatevent); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	if chatevent.RoomID == "" {
		return fmt.Errorf("room id is empty")
	}

	info, err := c.manager.repo.GetMeetings(ctx, chatevent.RoomID, "")
	if err != nil {
		return fmt.Errorf("rooms not found: %v", err)
	} else if len(info) == 0 {
		return fmt.Errorf("cant get room")
	}

	list, err := c.manager.repo.GetAttends(ctx, chatevent.RoomID)
	if err != nil {
		return fmt.Errorf("rooms not found: %v", err)
	}

	outgoingEvent, err := preparedDataToSend(c, list, chatevent.RoomID, info[0].AccessID, EventSomeOneJoin)
	if err != nil {
		return fmt.Errorf("error when prepared data: %v", err)
	}

	userGoing, err := preparedDataToSend(c, list, chatevent.RoomID, info[0].AccessID, EventCallUser)
	if err != nil {
		return fmt.Errorf("error when prepared data: %v", err)
	}

	for client := range c.manager.clients {
		if client.user.MeetingID == c.user.MeetingID {
			if client.connection == c.connection {
				client.egress <- userGoing
			} else {
				client.egress <- outgoingEvent
			}
		}

	}

	return nil
}

func SomeoneDisconnected(event Event, c *Client) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var someonedc someoneDisconnected
	if err := json.Unmarshal(event.Payload, &someonedc); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	if someonedc.RoomID == "" {
		return fmt.Errorf("room id is empty")
	}

	userID, err := uuid.Parse(someonedc.UserID)
	if err != nil {
		return fmt.Errorf("cannot parse uuid: %v", err)
	}

	logger.Info(fmt.Sprintf("deleting attends, %v", userID))
	if err := c.manager.repo.DeleteAttends(ctx, someonedc.RoomID, userID); err != nil {
		return err
	}

	var message NewSomeoneDisconnedted
	message.UserID = userID.String()

	data, err := json.Marshal(message)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	var outgoingEvent Event
	outgoingEvent.Payload = data
	outgoingEvent.Type = EventSomeOneOut

	for client := range c.manager.clients {
		if client.user.MeetingID == c.user.MeetingID {
			if client.connection != c.connection {
				client.egress <- outgoingEvent
			}
		}

	}

	return nil
}

func HostCleanUp(event Event, c *Client) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	var someonedc someoneDisconnected
	if err := json.Unmarshal(event.Payload, &someonedc); err != nil {
		return fmt.Errorf("bad payload in request: %v", err)
	}

	if someonedc.RoomID == "" && someonedc.UserID == "" {
		return fmt.Errorf("room id and user id is empty")
	}

	if someonedc.Role == host {
		logger.Info("host clean up a meetings")
		return backoff.Retry(func() error {
			if err := c.manager.repo.DeleteMeetings(ctx, someonedc.RoomID); err != nil {
				return err
			}

			id, err := uuid.Parse(someonedc.UserID)
			if err != nil {
				return err
			}

			part := map[string]interface{}{
				"done_at": time.Now(),
			}
			if err := c.manager.repo.UpdateMeetingsLogs(ctx, id, someonedc.RoomID, part); err != nil {
				return err
			}

			return nil
		}, backoff.NewExponentialBackOff())

	}

	for client := range c.manager.clients {
		if client.user.MeetingID == c.user.MeetingID {
			c.manager.removeClient(client)
		}

	}

	return nil
}

func preparedDataToSend(c *Client, attends []string, roomID, accessID, eventType string) (Event, error) {

	var broadMessage broadcastNewMsg

	broadMessage.Sent = time.Now()
	broadMessage.RoomID = roomID
	broadMessage.AccessID = accessID
	broadMessage.ListClient = attends

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return Event{}, fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	var outgoingEvent Event
	outgoingEvent.Payload = data
	outgoingEvent.Type = eventType

	return outgoingEvent, nil
}
