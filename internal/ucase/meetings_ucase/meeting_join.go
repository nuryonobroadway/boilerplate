package meetingsucase

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/otp"
)

// WebRTC (Web Real-Time Communications) merupakan sebuah proyek
// open-source yang memungkinkan untuk dilakukannya komunikasi real-time melalui web browser.

// PeerJS adalah library JavaScript yang menyederhanakan data WebRTC yang berupa peer-to-peer, video,
// dan panggilan audio. Jadi, dengan menggunakan PeerJS akan memudahkan kita dalam membuat aplikasinya.

type meetingJoin struct {
	config   *appctx.Config
	repo     repositories.Query
	handlers map[string]EventHandler
	clients  ClientList
	sync.RWMutex
	otps otp.RetentionMap
}

var (
	ErrEventNotSupported = errors.New("this event type is not supported")
)

func NewMeetingJoin(config *appctx.Config, repo repositories.Query, otps otp.RetentionMap) *meetingJoin {

	m := &meetingJoin{
		config:   config,
		repo:     repo,
		clients:  make(ClientList),
		handlers: make(map[string]EventHandler),
		otps:     otps,
	}
	m.setupEventHandlers()
	return m
}

// anyone can join
func (t *meetingJoin) Serve(d *appctx.Data) {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username", "user_id", "access_id", "room_id")
	if err != nil {
		logger.Error(err)
		return
	}

	username := path[0]
	user_id := path[1]
	access_id := path[2]
	meeting_id := path[3]
	

	_, err, valid := validator.IsValidUUID(user_id)
	if !valid && err != nil {
		logger.Error(err)
		return

	}

	user, err := t.repo.GetUser(d.Request.Context(), username)
	if err != nil {
		logger.Error(err)
		return
	}

	if t.repo.CheckHost(d.Request.Context(), user.ID, role_viewer) {
		otp := d.Request.URL.Query().Get("otp")

		logger.Info(otp)
		if otp == "" {
			// Tell the user its not authorized
			logger.Error(fmt.Errorf("otp empty"))

			return

		}

		// Verify OTP is existing
		if !t.otps.VerifyOTP(otp) {
			logger.Error(fmt.Errorf("status unauthorized"))
			return

		}
	}

	var attend *entity.Attend
	if t.repo.CheckAttends(d.Request.Context(), user.ID) {
		change := map[string]interface{}{
			"access_id": access_id,
		}
		attend, err = t.repo.UpdateAttends(d.Request.Context(), meeting_id, user.ID, change)
		if err != nil {
			logger.Error(err)
			return
		}
	} else {
		logger.Error(fmt.Errorf("user not join a room"))
		return
	}

	client := NewClient(d.Websocket, t, attend)
	t.addClient(client)

	go client.readMessages()
	go client.writeMessages()

}

// setupEventHandlers configures and adds all handlers
func (t *meetingJoin) setupEventHandlers() {
	t.handlers[EventSendMessage] = SendMessageHandler
	t.handlers[EventSomeOneJoin] = SomeoneJoin
	t.handlers[EventHostConnect] = HostConnected
	t.handlers[EventSomeOneOut] = SomeoneDisconnected
	t.handlers[EventHostDestroyedCall] = HostDestroyedCall
	t.handlers[EventCleanUpMeetings] = HostCleanUp
	// t.handlers[EventSomeOneOut] =
}

// routeEvent is used to make sure the correct event goes into the correct handler
func (t *meetingJoin) routeEvent(event Event, c *Client) error {
	// Check if Handler is present in Map
	if handler, ok := t.handlers[event.Type]; ok {
		// Execute the handler and return any err
		if err := handler(event, c); err != nil {
			return err
		}
		return nil
	} else {
		return ErrEventNotSupported
	}
}

// addClient will add clients to our clientList
func (t *meetingJoin) addClient(client *Client) {
	// Lock so we can manipulate
	t.Lock()
	defer t.Unlock()

	// Add Client
	t.clients[client] = true
}

// removeClient will remove the client and clean up
func (t *meetingJoin) removeClient(client *Client) {
	t.Lock()
	defer t.Unlock()

	// Check if Client exists, then delete it
	if _, ok := t.clients[client]; ok {
		// close connection
		if err := client.connection.Close(); err != nil {
			logger.Fatal("cant close websocket")
		}
		// remove
		delete(t.clients, client)
	}
}
