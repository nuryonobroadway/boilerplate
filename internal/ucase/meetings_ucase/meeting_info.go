package meetingsucase

import (
	"net/http"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type meetingInfo struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewMeetingInfo(config *appctx.Config, repo repositories.Query) *meetingInfo {
	return &meetingInfo{config, repo}
}

func (t *meetingInfo) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username", "meeting_id")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	// username := path[0]
	meeting_id := path[1]

	// pass := d.Request.Header.Get(httpclient.Authorization)
	// token := strings.Fields(pass)[1]

	// claims, err := helper.ExtractAccessToken(t.config, token)
	// if err != nil {
	// 	return *appctx.NewResponse().
	// 		WithCode(consts.CodeInternalServerError).
	// 		WithError(err)
	// }

	// if claims.Username != username {
	// 	return *appctx.NewResponse().
	// 		WithCode(consts.CodeForbidden).
	// 		WithError(fmt.Errorf("username not same"))
	// }

	// text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	// _, err, valid := validator.IsValidUUID(text)
	// if !valid && err != nil {
	// 	return *appctx.NewResponse().
	// 		WithCode(consts.CodeForbidden).
	// 		WithMessage(err.Error())
	// }

	info, err := t.repo.MeetingInfo(d.Request.Context(), meeting_id, "")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithData(info)
}
