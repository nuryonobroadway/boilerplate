package meetingsucase

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"golang.org/x/crypto/bcrypt"
)

type meetingCreate struct {
	config *appctx.Config
	repo   repositories.Query
}

const (
	role_host   = 3
	role_viewer = 4
)

func NewMeetingCreate(config *appctx.Config, repo repositories.Query) *meetingCreate {
	return &meetingCreate{config, repo}
}

// create only can use for authenticated user
func (t *meetingCreate) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err.Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	username := path[0]
	meeting := entity.Meeting{}
	if err := d.Cast(&meeting); err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}
	logger.Info(meeting)
	if claims.Username != username {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(fmt.Errorf("username not same").Error())
	}

	user, err := t.repo.FindUser(d.Request.Context(), id, claims.Username, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeNotFound).
			WithError(err.Error())
	}

	password, err := bcrypt.GenerateFromPassword([]byte(meeting.RoomPassword), 10)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithMessage(err.Error())
	}

	logger.Info(meeting)
	meetings, err := t.repo.InsertMeetings(
		d.Request.Context(),
		user.ID,
		helper.BuilderMeetings(helper.RandomString(), "", meeting.RoomName, string(password)))
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	_, err = t.repo.InsertAttends(d.Request.Context(), helper.BuilderAttends(role_host, meetings.ID, "", user.ID))
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	if err := t.repo.InsertMeetingsLogs(d.Request.Context(), helper.BuilderMeetingLogs(id, meetings.ID, meetings.RoomName)); err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	response := map[string]interface{}{
		"meeting_id": meetings.ID,
		"user_id":    user.ID,
		"username":   user.Username,
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithData(response)
}
