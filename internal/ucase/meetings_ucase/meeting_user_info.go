package meetingsucase

import (
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type meetingUserInfo struct {
	config *appctx.Config
	repo   repositories.Query
}

func NewMeetingUserInfo(config *appctx.Config, repo repositories.Query) *meetingUserInfo {
	return &meetingUserInfo{config, repo}
}

func (t *meetingUserInfo) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username", "meeting_id")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err.Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	// username := path[0]
	meeting_id := path[1]

	info, err := t.repo.GetUserAndRoomInfo(d.Request.Context(), meeting_id, id)
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(http.StatusInternalServerError).
			WithError(err)
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithData(info)
}
