package meetingsucase

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/helper"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/validator"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/httpclient"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/otp"
	"golang.org/x/crypto/bcrypt"
)

/*
type userLoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var req userLoginRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Authenticate user / Verify Access token, what ever auth method you use
	if req.Username == "percy" && req.Password == "123" {
		// format to return otp in to the frontend
		type response struct {
			OTP string `json:"otp"`
		}

		// add a new OTP
		otp := m.otps.NewOTP()

		resp := response{
			OTP: otp.Key,
		}

		data, err := json.Marshal(resp)
		if err != nil {
			log.Println(err)
			return
		}
		// Return a response to the Authenticated user with the OTP
		w.WriteHeader(http.StatusOK)
		w.Write(data)
		return
	}

	// Failure to auth
	w.WriteHeader(http.StatusUnauthorized)
*/

type meetingLogin struct {
	config *appctx.Config
	repo   repositories.Query
	otps   otp.RetentionMap
}

func NewMeetingLogin(config *appctx.Config, repo repositories.Query, otps otp.RetentionMap) *meetingLogin {
	return &meetingLogin{config, repo, otps}
}

// create only can use for authenticated user
func (t *meetingLogin) Serve(d *appctx.Data) appctx.Response {
	path, err := validator.ParameterIsExist(d.Request, 0, []string{}, "username")
	if err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	username := path[0]
	pass := d.Request.Header.Get(httpclient.Authorization)
	token := strings.Fields(pass)[1]

	claims, err := helper.ExtractAccessToken(t.config, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeInternalServerError).
			WithError(err.Error())
	}

	if claims.Username != username {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(fmt.Errorf("username not same").Error())
	}

	text := helper.DecryptText(t.config.App.HexKey, claims.ID)
	id, err, valid := validator.IsValidUUID(text)
	if !valid && err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithError(err.Error())
	}

	meeting := entity.MeetingLogin{}
	if err := d.Cast(&meeting); err != nil {
		logger.Error(err)
		return *appctx.NewResponse().
			WithCode(consts.CodeBadRequest).
			WithError(err)
	}

	user, err := t.repo.FindUser(d.Request.Context(), id, claims.Username, token)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeNotFound).
			WithMessage(err.Error())
	}

	m, err := t.repo.GetMeetings(d.Request.Context(), meeting.MeetingID, meeting.RoomName)
	if err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeNotFound).
			WithMessage(err.Error())
	}

	room_meeting := m[0]
	if err := bcrypt.CompareHashAndPassword([]byte(room_meeting.RoomPassword), []byte(meeting.RoomPassword)); err != nil {
		return *appctx.NewResponse().
			WithCode(consts.CodeForbidden).
			WithMessage(err.Error())
	}

	if !t.repo.CheckAttends(d.Request.Context(), user.ID) {
		_, err = t.repo.InsertAttends(d.Request.Context(), helper.BuilderAttends(role_viewer, room_meeting.ID, "", user.ID))
		if err != nil {
			logger.Error(err)
			return *appctx.NewResponse().
				WithCode(http.StatusInternalServerError).
				WithError(err)
		}
	}

	response := map[string]interface{}{
		"meeting_id": room_meeting.ID,
		"user_id":    user.ID,
		"username":   username,
		"otp":        t.otps.NewOTP(),
	}

	return *appctx.NewResponse().
		WithCode(http.StatusOK).
		WithData(response)
}
