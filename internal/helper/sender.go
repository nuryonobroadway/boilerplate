package helper

import (
	"encoding/json"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/messagebroker"
)

func Sender(conn messagebroker.Connection, infoType string, message interface{}) error {
	e := entity.Logger{
		LogType: infoType,
		Message: message,
	}

	data, err := json.Marshal(&e)
	if err != nil {
		return err
	}

	return conn.Send(data)
}
