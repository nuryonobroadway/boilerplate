package helper

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
)

type UserClaims struct {
	jwt.StandardClaims
	ID        string    `json:"id,omitempty"`
	Username  string    `json:"username,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty"`
}

func GenerateToken(config *appctx.Config, duration time.Time, user *entity.User) (string, error) {
	claims := UserClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: duration.Unix(),
		},
		Username:  user.Username,
		ID:        EncryptText(config.App.HexKey, user.ID.String()),
		CreatedAt: time.Now(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(config.App.ApiKey))
}

func VerifyToken(config *appctx.Config, token string) (*jwt.Token, error) {
	t, err := jwt.ParseWithClaims(
		token,
		&UserClaims{},
		func(t *jwt.Token) (interface{}, error) {
			_, ok := t.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("unexpected token signing method")
			}
			return []byte(config.App.ApiKey), nil
		},
	)
	if err != nil {
		return nil, fmt.Errorf("invalid token: %v", err)
	}

	return t, nil
}

func ExtractAccessToken(config *appctx.Config, token string) (*UserClaims, error) {
	t, err := VerifyToken(config, token)
	if err != nil {
		return nil, fmt.Errorf("invalid token: %v", err)
	}

	claims, ok := t.Claims.(*UserClaims)
	if !ok {
		return nil, fmt.Errorf("invalid token claims")
	}

	return claims, nil
}
