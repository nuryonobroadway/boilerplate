package helper

import (
	"time"

	"github.com/google/uuid"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
)

func BuilderMeetings(room_id, access_id, room_name, room_password string) *entity.Meeting {
	return &entity.Meeting{
		ID:           room_id,
		RoomName:     room_name,
		RoomPassword: room_password,
		AccessID:     access_id,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
		DeletedAt:    time.Time{},
	}
}

func BuilderMeetingLogs(user_id uuid.UUID, room_id, room_name string) *entity.MeetingLogs {
	return &entity.MeetingLogs{
		ID:          uuid.New(),
		UserID:      user_id,
		MeetingID:   room_id,
		MeetingName: room_name,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		DeletedAt:   time.Time{},
	}
}

func BuilderAttends(role int, meeting_id, access_id string, user_id uuid.UUID) *entity.Attend {
	return &entity.Attend{
		ID:        uuid.New(),
		UserID:    user_id,
		MeetingID: meeting_id,
		AccessID:  access_id,
		RoleID:    role,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		DeletedAt: time.Time{},
	}
}
