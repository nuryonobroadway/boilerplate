package entity

import (
	"time"

	"github.com/google/uuid"
)

type (
	User struct {
		ID        uuid.UUID `json:"id,omitempty" db:"id,omitempty"`
		Username  string    `json:"username" db:"username,omitempty"`
		Password  string    `json:"password" db:"password,omitempty"`
		RoleID    int       `json:"role_id,omitempty" db:"role_id,omitempty"`
		CreatedAt time.Time `json:"created_at" db:"created_at,omitempty" `
		UpdatedAt time.Time `json:"updated_at" db:"updated_at,omitempty"`
		DeletedAt time.Time `json:"deleted_at" db:"deleted_at,omitempty"`
	}

	Session struct {
		ID           uuid.UUID `json:"id,omitempty" db:"id"`
		UserID       uuid.UUID `json:"user_id,omitempty" db:"user_id"`
		LoginAt      time.Time `json:"login_at,omitempty" db:"login_at"`
		LastActivity time.Time `json:"last_activity,omitempty" db:"last_activity"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}

	Meeting struct {
		ID           string    `json:"id,omitempty" db:"id"`
		AccessID     string    `json:"access_id,omitempty" db:"access_id"`
		RoomName     string    `json:"room_name,omitempty" db:"room_name"`
		RoomPassword string    `json:"room_password,omitempty" db:"room_password"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}

	MeetingLogs struct {
		ID          uuid.UUID `json:"id,omitempty" db:"id"`
		UserID      uuid.UUID `json:"user_id,omitempty" db:"user_id"`
		MeetingID   string    `json:"meeting_id,omitempty" db:"meeting_id"`
		MeetingName string    `json:"meeting_name,omitempty" db:"meeting_name"`
		DoneAt      time.Time `json:"done_at" db:"done_at"`
		CreatedAt   time.Time `json:"created_at" db:"created_at" `
		UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt   time.Time `json:"deleted_at" db:"deleted_at"`
	}

	Attend struct {
		ID        uuid.UUID `json:"id,omitempty" db:"id"`
		UserID    uuid.UUID `json:"user_id,omitempty" db:"user_id"`
		MeetingID string    `json:"meeting_id,omitempty" db:"meeting_id"`
		AccessID  string    `json:"access_id,omitempty" db:"access_id"`
		RoleID    int       `json:"role_id,omitempty" db:"role_id"`
		CreatedAt time.Time `json:"created_at" db:"created_at" `
		UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt time.Time `json:"deleted_at" db:"deleted_at"`
	}

	UserInRoomInfomation struct {
		ID        uuid.UUID `json:"id,omitempty" db:"id"`
		MeetingID string    `json:"meeting_id,omitempty" db:"meeting_id"`
		UserID    uuid.UUID `json:"user_id,omitempty" db:"user_id"`
		Username  string    `json:"username,omitempty" db:"username"`
		RoleName  string    `json:"name,omitempty" db:"name"`
		CreatedAt time.Time `json:"created_at" db:"created_at" `
		UpdatedAt time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt time.Time `json:"deleted_at" db:"deleted_at"`
	}

	Tokens struct {
		ID          uuid.UUID `json:"id,omitempty" db:"id"`
		SessionID   uuid.UUID `json:"session_id,omitempty" db:"session_id"`
		AccessToken string    `json:"access_token,omitempty" db:"access_token"`
		ExpiredAt   time.Time `json:"expired_at,omitempty" db:"expired_at"`
		CreatedAt   time.Time `json:"created_at" db:"created_at" `
		UpdatedAt   time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt   time.Time `json:"deleted_at" db:"deleted_at"`
	}

	GeneratedMeeting struct {
		ID        int    `json:"id,omitempty"`
		MeetingID string `json:"meeting_id,omitempty"`
		Status    string `json:"status,omitempty"`
	}

	Logger struct {
		LogType string      `json:"log_type"`
		Message interface{} `json:"message"`
	}

	ListAccessID struct {
		AccessID []string `json:"access_id"`
	}

	MeetingLogin struct {
		MeetingID    string `json:"meeting_id,omitempty" db:"meeting_id"`
		RoomName     string `json:"room_name,omitempty" db:"room_name"`
		RoomPassword string `json:"room_password,omitempty" db:"room_password"`
	}
)
