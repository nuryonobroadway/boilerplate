package entity

import (
	"time"

	"github.com/google/uuid"
)

type (
	Client struct {
		ID           uuid.UUID `json:"uuid" db:"id"`
		Name         string    `json:"name" db:"name"`
		Description  string    `json:"description" db:"description"`
		PrivyIdOwner string    `json:"privy_id_owner" db:"privy_id_owner"`
		IsActive     bool      `json:"is_active" db:"is_active"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}

	ClientApiKey struct {
		ID           uuid.UUID `json:"uuid" db:"id"`
		Name         string    `json:"name" db:"name"`
		ApiKeyId     string    `json:"api_key_id" db:"api_key_id"`
		ApiKeySecret string    `json:"api_key_secret" db:"api_key_secret"`
		IsActive     bool      `json:"is_active" db:"is_active"`
		CLientId     uuid.UUID `json:"client_id" db:"client_id"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}

	ClientRequestLog struct {
		ID                 uuid.UUID `json:"uuid" db:"uuid"`
		ApiKeyId           string    `json:"api_key_id" db:"api_key_id"`
		VendorFeatureId    uuid.UUID `json:"vendor_feature_id" db:"vendor_feature_id"`
		ClientRequestData  string    `json:"client_request_data" db:"client_request_data"`
		ClientResponseData string    `json:"client_response_data" db:"client_response_data"`
		HttpStatusCode     int       `json:"http_status_code" db:"http_status_code"`
		HttpMethod         string    `json:"http_method" db:"http_method"`
		TransactionId      uuid.UUID `json:"transaction_id" db:"transaction_id"`
		CLientId           uuid.UUID `json:"client_id" db:"client_id"`
		CreatedAt          time.Time `json:"created_at" db:"created_at" `
		UpdatedAt          time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt          time.Time `json:"deleted_at" db:"deleted_at"`
	}
)
