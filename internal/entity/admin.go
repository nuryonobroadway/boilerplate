package entity

import (
	"time"

	"github.com/google/uuid"
)

type (
	SuperAdmin struct {
		ID           uuid.UUID `json:"uuid" db:"uuid"`
		PrivyIdAdmin string    `json:"privy_id_admin" db:"privy_id_admin"`
		IsActive     bool      `json:"is_active" db:"is_active"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}

	Admin struct {
		ID           uuid.UUID `json:"uuid" db:"uuid"`
		PrivyIdAdmin string    `json:"privy_id_admin" db:"privy_id_admin"`
		AddedBy      uuid.UUID `json:"added_by" db:"added_by"`
		IsActive     bool      `json:"is_active" db:"is_active"`
		CreatedAt    time.Time `json:"created_at" db:"created_at" `
		UpdatedAt    time.Time `json:"updated_at" db:"updated_at"`
		DeletedAt    time.Time `json:"deleted_at" db:"deleted_at"`
	}
)
