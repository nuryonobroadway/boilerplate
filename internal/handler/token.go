package handler

import (
	"context"
	"time"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type tokenCheck struct {
	repo   repositories.Query
	config *appctx.Config
	token  chan entity.Tokens
}

func NewTokenCheck(config *appctx.Config, repo repositories.Query) {
	t := &tokenCheck{
		repo:   repo,
		config: config,
		token:  make(chan entity.Tokens),
	}

	go t.Check()
	go t.Execution()
}

func (t *tokenCheck) Check() {
	ticker := time.NewTicker(10 * time.Second)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for {
		select {
		case <-ticker.C:
			tokens, err := t.repo.AllTokens(ctx)
			if err != nil {
				logger.Error(err)
				return
			}

			if len(tokens) != 0 {
				for _, token := range tokens {
					t.token <- token
				}
			}

		case <-ctx.Done():
			logger.Error("context is done")
			return
		}
	}

}

func (t *tokenCheck) Execution() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for {
		select {
		case token, ok := <-t.token:
			if !ok {
				logger.Error("unexpected error")
				return
			}

			if time.Now().After(token.ExpiredAt) {
				logger.Info("found expired token")
				if err := t.repo.DeleteSessionsAndTokens(ctx, "id", token.SessionID); err != nil {
					logger.Error(err)
					return
				}
			}
		case <-ctx.Done():
			logger.Error("context is done")
			return

		}
	}
}
