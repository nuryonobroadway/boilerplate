// Package router
package router

import (
	"net/http"

	"github.com/gorilla/websocket"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/routerkit"
)

// httpHandlerFunc is a contract http handler for router
type httpHandlerFunc func(request *http.Request, svc contract.UseCase, conf *appctx.Config) appctx.Response

type httpSocketHandlerFunc func(request *http.Request, svc contract.UseSocketCase, conn *websocket.Conn, conf *appctx.Config)

// Router is a contract router and must implement this interface
type Router interface {
	Route() *routerkit.Router
}
