// Package router
package router

import (
	"context"
	"encoding/json"
	"net/http"
	"runtime/debug"
	"time"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/bootstrap"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/handler"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/middleware"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase"
	meetingsucase "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/meetings_ucase"
	usersucase "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/users_ucase"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/otp"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/routerkit"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/socket"

	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/mariadb"
	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/repositories"
	//"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/example"

	ucaseContract "gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
)

type router struct {
	config *appctx.Config
	router *routerkit.Router
}

// NewRouter initialize new router wil return Router Interface
func NewRouter(cfg *appctx.Config) Router {
	bootstrap.RegistryMessage()
	bootstrap.RegistryLogger(cfg)

	return &router{
		config: cfg,
		router: routerkit.NewRouter(routerkit.WithServiceName(cfg.App.AppName)),
	}
}

func (rtr *router) handle(hfn httpHandlerFunc, svc ucaseContract.UseCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				w.WriteHeader(http.StatusInternalServerError)
				res := appctx.Response{
					Code: consts.CodeInternalServerError,
				}

				res.GenerateMessage()
				logger.Error(logger.MessageFormat("error %v", string(debug.Stack())))
				json.NewEncoder(w).Encode(res)
				return
			}
		}()

		ctx := context.WithValue(r.Context(), "access", map[string]interface{}{
			"path":      r.URL.Path,
			"remote_ip": r.RemoteAddr,
			"method":    r.Method,
		})

		req := r.WithContext(ctx)

		if status := middleware.FilterFunc(rtr.config, req, mdws); status != 200 {
			rtr.response(w, appctx.Response{
				Code: status,
			})

			return
		}

		resp := hfn(req, svc, rtr.config)
		resp.Lang = rtr.defaultLang(req.Header.Get(consts.HeaderLanguageKey))
		rtr.response(w, resp)
	}
}

func (rtr *router) handleSocket(hfn httpSocketHandlerFunc, svc ucaseContract.UseSocketCase, mdws ...middleware.MiddlewareFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			err := recover()
			if err != nil {
				w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)
				w.WriteHeader(http.StatusInternalServerError)
				res := appctx.Response{
					Code: consts.CodeInternalServerError,
				}

				res.GenerateMessage()
				logger.Error(logger.MessageFormat("error %v", string(debug.Stack())))
				json.NewEncoder(w).Encode(res)
				return
			}
		}()

		ctx := context.WithValue(r.Context(), "access", map[string]interface{}{
			"path":      r.URL.Path,
			"remote_ip": r.RemoteAddr,
			"method":    r.Method,
		})

		req := r.WithContext(ctx)

		if status := middleware.FilterFunc(rtr.config, req, mdws); status != 200 {
			rtr.response(w, appctx.Response{
				Code: status,
			})

			return
		}

		conn, err := socket.Socket(w, r)
		if err != nil {
			logger.Fatal(err)
		}

		hfn(req, svc, conn, rtr.config)
	}
}

// response prints as a json and formatted string for DGP legacy
func (rtr *router) response(w http.ResponseWriter, resp appctx.Response) {

	w.Header().Set(consts.HeaderContentTypeKey, consts.HeaderContentTypeJSON)

	defer func() {
		resp.GenerateMessage()
		w.WriteHeader(resp.GetCode())
		json.NewEncoder(w).Encode(resp)
	}()
}

// Route preparing http router and will return mux router object
func (rtr *router) Route() *routerkit.Router {

	root := rtr.router.PathPrefix("/").Subrouter()
	//in := root.PathPrefix("/in/").Subrouter()
	liveness := root.PathPrefix("/").Subrouter()
	ex := root.PathPrefix("/external").Subrouter()
	in := root.PathPrefix("/v1/auth").Subrouter()
	out := root.PathPrefix("/v1/service").Subrouter()
	//inV1 := in.PathPrefix("/v1/").Subrouter()

	// open tracer setup
	bootstrap.RegistryOpenTracing(rtr.config)

	// db := bootstrap.RegistryMariaMasterSlave(rtr.config.WriteDB, rtr.config.ReadDB)
	db := bootstrap.RegistryPostgreDB(rtr.config.ReadDB, rtr.config.App.Timezone)
	user := repositories.NewClient(db, *rtr.config)
	// use case
	healthy := ucase.NewHealthCheck()
	token := ucase.NewToken(rtr.config)
	otps := otp.NewRetentionMap(context.Background(), time.Duration(int64(rtr.config.App.OtpTimeout))*time.Second)

	// healthy
	liveness.HandleFunc("/liveness", rtr.handle(
		handler.HttpRequest,
		healthy,
	)).Methods(http.MethodGet)

	// this is use case for example purpose, please delete
	//repoExample := repositories.NewExample(db)
	//el := example.NewExampleList(repoExample)
	//ec := example.NewPartnerCreate(repoExample)
	//ed := example.NewExampleDelete(repoExample)

	// TODO: create your route here

	// this route for example rest, please delete
	// example list
	//inV1.HandleFunc("/example", rtr.handle(
	//    handler.HttpRequest,
	//    el,
	//)).Methods(http.MethodGet)

	//inV1.HandleFunc("/example", rtr.handle(
	//    handler.HttpRequest,
	//    ec,
	//)).Methods(http.MethodPost)

	//inV1.HandleFunc("/example/{id:[0-9]+}", rtr.handle(
	//    handler.HttpRequest,
	//    ed,
	//)).Methods(http.MethodDelete)

	// make a handle to check token is invalid or not
	handler.NewTokenCheck(rtr.config, user)

	ex.HandleFunc("/generate/token", rtr.handle(
		handler.HttpRequest,
		token,
		middleware.ValidateSignature,
	)).Methods(http.MethodGet)

	in.HandleFunc("/register", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserRegister(rtr.config, user),
		// middleware.ValidateSignature,
	)).Methods(http.MethodPost)

	in.HandleFunc("/login", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserLogin(rtr.config, user),
		// middleware.ValidateSignature,
	)).Methods(http.MethodPost)

	in.HandleFunc("/logout/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserLogout(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/profile", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserProfile(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/sessions/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserSession(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	in.HandleFunc("/change_password/{username}", rtr.handle(
		handler.HttpRequest,
		usersucase.NewUserChangePassword(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodPatch)

	out.HandleFunc("/create/{username}", rtr.handle(
		handler.HttpRequest,
		meetingsucase.NewMeetingCreate(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodPost)

	out.HandleFunc("/meeting/logs/{username}", rtr.handle(
		handler.HttpRequest,
		meetingsucase.NewMeetingUserLogs(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	out.HandleFunc("/info/{username}/{meeting_id}", rtr.handle(
		handler.HttpRequest,
		meetingsucase.NewMeetingUserInfo(rtr.config, user),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodGet)

	// out.HandleFunc("/info/{username}/{room_id}", rtr.handle(
	// 	handler.HttpRequest,
	// 	meetingsucase.NewMeetingInfo(rtr.config, user),
	// 	// middleware.ValidateSignature,
	// 	// middleware.ValidateToken,
	// )).Methods(http.MethodGet)

	out.HandleFunc("/login/{username}", rtr.handle(
		handler.HttpRequest,
		meetingsucase.NewMeetingLogin(rtr.config, user, otps),
		// middleware.ValidateSignature,
		middleware.ValidateToken,
	)).Methods(http.MethodPost)

	out.HandleFunc("/join/{username}/{user_id}/{access_id}/{room_id}", rtr.handleSocket(
		handler.HttpSocketRequest,
		meetingsucase.NewMeetingJoin(rtr.config, user, otps),
		// middleware.ValidateSignature,
		// middleware.ValidateToken,
	)).Methods(http.MethodGet)

	// rtr.router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// 	log.Println("here")
	// 	http.ServeFile(w, r, "/home/slvr/privypass-hash-signing-se/internal/frontend/index.html")
	// })

	// out.HandleFunc("/ws", rtr.handle(
	// 	handler.HttpRequest,
	// 	socketucase.NewManager(),
	// 	// middleware.ValidateSignature,
	// 	// middleware.ValidateToken,
	// ))

	return rtr.router

}

func (rtr *router) defaultLang(l string) string {

	if len(l) == 0 {
		return rtr.config.App.DefaultLang
	}

	return l
}
