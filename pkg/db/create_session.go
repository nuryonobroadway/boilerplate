// Package mariadb
package db

import (
	"fmt"
	"net/url"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// CreateSession create new session maria db
func CreateSession(cfg *Config, driver string) (*sqlx.DB, error) {
	if len(strings.Trim(cfg.Charset, "")) == 0 {
		cfg.Charset = "UTF8"
	}

	param := url.Values{}
	param.Add("timeout", fmt.Sprintf("%v", cfg.Timeout))
	param.Add("charset", cfg.Charset)
	param.Add("parseTime", "True")
	param.Add("loc", cfg.TimeZone)

	var db *sqlx.DB
	var err error

	switch driver {
	case "mysql":
		connStr := fmt.Sprintf(ConnStringTemplate,
			cfg.User,
			cfg.Password,
			cfg.Host,
			cfg.Port,
			cfg.Name,
			param.Encode(),
		)

		db, err = sqlx.Open(driver, connStr)
		if err != nil {
			return db, err
		}

	case "postgres":
		psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
			"password=%s dbname=%s sslmode=disable",
			cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.Name)

		db, err = sqlx.Open(driver, psqlInfo)
		if err != nil {
			return db, err
		}

	}

	db.SetMaxOpenConns(cfg.MaxOpenConns)
	db.SetMaxIdleConns(cfg.MaxIdleConns)
	db.SetConnMaxLifetime(cfg.MaxLifetime)

	return db, nil
}
