// Package postgresdb
package postgresdb

import (
	"context"
	"database/sql"
	"fmt"
	"sync"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/db"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/tracer"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/util"
)

type postreMasterSlave struct {
	db      *sqlx.DB
	dbRead  *sqlx.DB
	cfg     *db.Config
	cfgRead *db.Config
}

// NewMariaMasterSlave initialize maria db for write read
func NewPostgreMasterSlave(cfgWrite *db.Config, cfgRead *db.Config) (db.Adapter, error) {
	x := postreMasterSlave{cfg: cfgWrite, cfgRead: cfgRead}

	e := x.initialize()

	return &x, e
}

func (d *postreMasterSlave) initialize() error {
	dbWrite, err := db.CreateSession(d.cfg, "mysql")

	if err != nil {
		return err
	}

	err = dbWrite.Ping()
	if err != nil {
		return err
	}

	dbRead, err := db.CreateSession(d.cfgRead, "mysql")
	if err != nil {
		return err
	}

	err = dbRead.Ping()
	if err != nil {
		return err
	}

	d.db = dbWrite
	d.dbRead = dbRead

	return nil
}

// QueryRow select single row database will return  sql.row raw
func (d *postreMasterSlave) QueryRow(ctx context.Context, query string, args ...interface{}) *sql.Row {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "query_row",
		tracer.WithResourceNameOptions(query),
		tracer.WithOptions("sql.query", query),
		tracer.WithOptions("sql.args", util.DumpToString(args)),
	)
	defer tracer.SpanFinish(ctx)
	return d.selector().QueryRowContext(ctx, query, args...)
}

// QueryRows select multiple rows of database will return  sql.rows raw
func (d *postreMasterSlave) QueryRows(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "query_rows",
		tracer.WithResourceNameOptions(query),
		tracer.WithOptions("sql.query", query),
		tracer.WithOptions("sql.args", util.DumpToString(args)),
	)
	defer tracer.SpanFinish(ctx)
	return d.selector().QueryContext(ctx, query, args...)
}

// Fetch select multiple rows of database will cast data to struct passing by parameter
func (d *postreMasterSlave) Fetch(ctx context.Context, dst interface{}, query string, args ...interface{}) error {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "fetch_rows",
		tracer.WithResourceNameOptions(query),
		tracer.WithOptions("sql.query", query),
		tracer.WithOptions("sql.args", util.DumpToString(args)),
	)
	defer tracer.SpanFinish(ctx)
	return d.selector().SelectContext(ctx, dst, query, args...)
}

// FetchRow fetching one row database will cast data to struct passing by parameter
func (d *postreMasterSlave) FetchRow(ctx context.Context, dst interface{}, query string, args ...interface{}) error {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "fetch_row",
		tracer.WithResourceNameOptions(query),
		tracer.WithOptions("sql.query", query),
		tracer.WithOptions("sql.args", util.DumpToString(args)),
	)
	defer tracer.SpanFinish(ctx)
	return d.selector().GetContext(ctx, dst, query, args...)
}

// Exec execute mysql command query
func (d *postreMasterSlave) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "exec",
		tracer.WithResourceNameOptions(query),
		tracer.WithOptions("sql.query", query),
		tracer.WithOptions("sql.args", util.DumpToString(args)),
	)
	defer tracer.SpanFinish(ctx)
	return d.db.ExecContext(ctx, query, args...)
}

// BeginTx start new transaction session
func (d *postreMasterSlave) BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error) {
	ctx = tracer.DBSpanStartWithOption(ctx, d.cfg.Name, "begin.transaction")
	defer tracer.SpanFinish(ctx)
	return d.db.BeginTx(ctx, opts)
}

// Ping check database connectivity
func (d *postreMasterSlave) Ping(ctx context.Context) error {
	return d.db.PingContext(ctx)
}

// HealthCheck checking healthy of database connection
func (d *postreMasterSlave) HealthCheck() error {
	var err1, err2 error
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		err1 = d.Ping(context.Background())
		wg.Done()
	}()

	if d.dbRead != nil {
		wg.Add(1)
		go func() {
			err2 = d.dbRead.PingContext(context.Background())
			wg.Done()
		}()
	}

	wg.Wait()

	if err1 != nil && err2 != nil {
		return fmt.Errorf("database write error:%s; database read error:%s; ", err1.Error(), err2.Error())
	}

	if err1 != nil {
		return fmt.Errorf("database write error:%s;", err1.Error())

	}

	if err2 != nil {
		return fmt.Errorf("database read error:%s;", err2.Error())

	}

	return nil

}

func (d *postreMasterSlave) selector() *sqlx.DB {
	if d.dbRead != nil {
		return d.dbRead
	}

	return d.db
}
