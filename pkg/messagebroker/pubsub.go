package messagebroker

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/wagslane/go-rabbitmq"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

type pubsub struct {
	conn   *rabbitmq.Conn
	config *appctx.Config
}

func NewPubSub(conn *rabbitmq.Conn, config *appctx.Config) Connection {
	return &pubsub{conn, config}
}

func (p *pubsub) Send(message []byte) error {
	defer p.conn.Close()

	publisher, err := rabbitmq.NewPublisher(
		p.conn,
		rabbitmq.WithPublisherOptionsLogging,
		rabbitmq.WithPublisherOptionsExchangeName(p.config.Rabbit.Exchange),
		rabbitmq.WithPublisherOptionsExchangeDeclare,
	)
	if err != nil {
		return err
	}
	defer publisher.Close()

	publisher.NotifyReturn(func(r rabbitmq.Return) {
		log.Printf("message returned from server: %s", string(r.Body))
	})

	publisher.NotifyPublish(func(c rabbitmq.Confirmation) {
		log.Printf("message confirmed from server. tag: %v, ack: %v", c.DeliveryTag, c.Ack)
	})

	// block main thread - wait for shutdown signal
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()

	fmt.Println("awaiting signal")

	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			err = publisher.Publish(
				message,
				[]string{p.config.Rabbit.Exchange},
				rabbitmq.WithPublishOptionsContentType(p.config.Rabbit.ContentType),
				rabbitmq.WithPublishOptionsMandatory,
				rabbitmq.WithPublishOptionsPersistentDelivery,
				rabbitmq.WithPublishOptionsExchange("events"),
			)
			if err != nil {
				return err
			}
		case <-done:
			fmt.Println("stopping publisher")
			return nil
		}
	}

}

func (p *pubsub) Receive() error {
	defer p.conn.Close()
	consumer, err := rabbitmq.NewConsumer(
		p.conn,
		func(d rabbitmq.Delivery) rabbitmq.Action {
			evt := entity.Logger{}

			err := json.Unmarshal(d.Body, &evt)
			if err != nil {
				logger.Error(err)
				return rabbitmq.NackRequeue
			}

			switch evt.LogType {
			case "info":
				logger.Info(evt.Message)
			case "error":
				logger.Error(evt.Message)
			}

			// rabbitmq.Ack, rabbitmq.NackDiscard, rabbitmq.NackRequeue
			return rabbitmq.Ack
		},
		"logs",
		rabbitmq.WithConsumerOptionsRoutingKey(p.config.Rabbit.RoutingKey),
		rabbitmq.WithConsumerOptionsExchangeName(p.config.Rabbit.Exchange),
		rabbitmq.WithConsumerOptionsExchangeDeclare,
	)
	if err != nil {
		log.Fatal(err)
	}
	defer consumer.Close()

	// block main thread - wait for shutdown signal
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		fmt.Println()
		fmt.Println(sig)
		done <- true
	}()

	fmt.Println("awaiting signal")
	<-done
	fmt.Println("stopping consumer")

	return nil
}
