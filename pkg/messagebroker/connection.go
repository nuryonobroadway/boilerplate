package messagebroker

type Connection interface {
	Send(message []byte) error
	Receive() error
}

// func NewConnection(conn *ampqp.Connection) (Connection, error) {
// 	e := fmt.Sprintf("amqp://%s:%s@%s/")
// 	conn, err := ampqp.Dial(e)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return &connection{conn}, nil
// }
