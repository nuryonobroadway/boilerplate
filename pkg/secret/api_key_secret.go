package secret

import (
	"crypto/rand"
	"fmt"
	"io"
)

const (
	apiKeyLength    = 16
	apiSecretLength = 32
)

func GenerateAPIKeyAndSecret() (string, string, error) {
	// Generate a new API key
	apiKeyBytes := make([]byte, apiKeyLength)
	if _, err := io.ReadFull(rand.Reader, apiKeyBytes); err != nil {
		return "", "", err
	}
	apiKey := fmt.Sprintf("%x", apiKeyBytes)

	// Generate a new API secret
	apiSecretBytes := make([]byte, apiSecretLength)
	if _, err := io.ReadFull(rand.Reader, apiSecretBytes); err != nil {
		return "", "", err
	}
	apiSecret := fmt.Sprintf("%x", apiSecretBytes)

	return apiKey, apiSecret, nil
}
