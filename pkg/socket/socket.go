package socket

import (
	"net/http"

	"github.com/gorilla/websocket"
)

var (
	wsUpgrader = websocket.Upgrader{
		CheckOrigin:     checkOrigin,
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

func Socket(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	return wsUpgrader.Upgrade(w, r, nil)
}

func checkOrigin(r *http.Request) bool {

	origin := r.Header.Get("Origin")
	switch origin {
	case "http://192.168.56.117:8000",
		"http://localhost:3000",
		"http://127.0.0.1:5500",
		"https://sweet-kashata-f1fadd.netlify.app",
		"http://localhost:5173",
		"https://incomparable-raindrop-ba0d0f.netlify.app":
		return true
	default:
		return false
	}
}
