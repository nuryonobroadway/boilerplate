package pusher

import (
	"github.com/wagslane/go-rabbitmq"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/messagebroker"
)

func StartLogger() {
	cfg, e := appctx.NewConfig()

	if e != nil {
		logger.Fatal(e)
	}

	uri := `amqp://` + cfg.Rabbit.User + `:` + cfg.Rabbit.Pass + `@` + cfg.Rabbit.Host + `/`
	logger.Info(uri)
	conn, err := rabbitmq.NewConn(
		uri,
		rabbitmq.WithConnectionOptionsLogging,
	)

	if err != nil {
		logger.Fatal(err)
	}

	messagebroker.NewPubSub(conn, cfg).Receive()
}
